/*
 * This Java source file was generated by the Gradle 'init' task.
 */
import redis.clients.jedis.Jedis; 
package sample;
public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());
        Jedis jedis = new Jedis("localhost");
        System.out.println("Connection to server successfully");
        System.out.println("Server is running: " + jedis.ping());
    }
}
